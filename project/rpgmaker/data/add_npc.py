# v0.1
import json
import os
import copy
import random

import shutil
from datetime import datetime

map_name = 'Map033.json'
npc_dialogs = [
    "You know, life in Terranova wasn't always this peaceful. We've come a long way since Malachi's attack.",
    "I used to be a trader before Malachi arrived. Now, I'm focused on rebuilding and cultivating the land.",
    "The Magic Orb... Some say it's just a legend, but I believe it holds the key to our civilization's prosperity.",
    "Wow, I heard Gabriel and Ava saved you guys from Malachi! I wish I could have their strenth.",
    "Entrance to Underworld? No one knows. But I think it should be around the Volcanos.",
    "I heard dark magic is the forbidden magic that no one should touch. It curses and kills.",
    "Crystal Village, once a bustling trading hub, reduced to ashes by Malachi. We're slowly rebuilding.",
    "Have you seen the crystalized tunnel? It's like something out of a nightmare. I'm glad it's cleared now.",
    "The Crystal Tunnel was our lifeline, connecting us to Haveston. QuickSilver's presence was a nightmare.",
    "You saved us from QuickSilver! and our business would be resumed now!",
    "I used to travel to Haveston for business. It breaks my heart to see the forest corruption affecting them.",
    "Woodstock and its evil purple venom... It's devouring everything in its path. We must find a way to stop it.",
    "Malachi's escape haunts us to this day and he's coming back. I hope we 4 heroes will once again fight against it.",
    "The polluted forest is an omen of doom. If we don't act soon, Haveston might crumble.",
    "I've heard rumors of Malachi's return. We must prepare ourselves for the dark days ahead.",
    "We don't know the name of the four heroes and no one'd ever seen these heroes. But their bravery is always remembered.",
    "Gabriel is very strong. Could he be the hero to save us from Malachi's attack?",
    "I think Gabriel and Ava are 2 of the 4 heroes! Who can ",
    "Malachi's return? I thought we were finally free from his tyranny. We must prepare for the worst.",
    "The pollution in the nearby forest threatens Haveston's prosperity. We must find a solution soon.",
    "I didn't see any travelers recently. But most travelers would just pass Haveston and go to the big city in the south.",
    "I saw you guys travels from Crystal tunnel! Is it cleared? Who is the hero behind?",
    "Gabriel is at the traveler's camp talking to people. You should talk to him before you go to Rotten Forest in the east.",
    "Woodstock corrupted the whole forest in the east. Everything there is poisonous. If you want to go there, buy some masks in the armor shop. ",
    "Have you ever seen venom slime and antidote slime, they seems to have different effect when you attack or touch them.",
    "Woodstock is actually two treants? Wood's storm blows, tornado's might, Stock strikes, physical, fearless fight. Wood's defense, magic untamed, Stock's defense, strength unchained.",
    "If you see butterfly in forest, you must defeat it first asap. It seals any skills!",
    "Shadow goblin can summon its shadows to fight for him. Don't be tricked but focus on the real one. Hmmmm, I cannot tell the real one apart.",
    "I heard there's a goblin lead in that contaminated forest. It's very tough. I don't think any of you can hurt it.",
    "If you see bomb creature, kill it at all cost in one shot. Otherwise, it explodes!",
    "I hate goblins, they are very annoying!",
    "Dark matter is weak to Holy magic. Don't forget that.",
    "A bold man carrying a huge sword? Never heard of such person. We barely have any travelers.",
    "We are from Crystal Village and stucked here because of QuickSilver. Now, thank you! The tunnel is cleared and we finally can return home.",
    "Kidos. Nice to see you well and sound. I was planning to clear the QuickSilver mess. But you guys get there and defeat it first, nice job! I actually have some urgent tasks now. I'll see you again! Also Ava wanted to say hi to you all as well. Oh, I think I forgot. Here's some useful items I think could be handy if you plan to go to eastern Rotten Forest.",

]




def create_backup_file(src_file, dest_dir):
    # Create destination directory if it doesn't exist
    os.makedirs(dest_dir, exist_ok=True)

    timestamp = datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]  # Include milliseconds
    dest_file = os.path.join(dest_dir, f"{timestamp}_{os.path.basename(src_file)}")
    shutil.copy2(src_file, dest_file)
    print(f"Backup created: {dest_file}")

NOTE_V0_1 = "Generated by add_npc.py v0.1"

NPC_TEMPLATE = {
  "id": -1,
  "name": "villager Generated",
  "note": NOTE_V0_1,
  "pages": [
    {
      "conditions": {
        "actorId": 1,
        "actorValid": False,
        "itemId": 1,
        "itemValid": False,
        "selfSwitchCh": "A",
        "selfSwitchValid": False,
        "switch1Id": 1,
        "switch1Valid": False,
        "switch2Id": 1,
        "switch2Valid": False,
        "variableId": 1,
        "variableValid": False,
        "variableValue": 0
      },
      "directionFix": False,
      "image": {
        "tileId": 0,
        "characterName": "People{}",
        "direction": 2,
        "pattern": 1,
        "characterIndex": 0
      },
      "list": [],
      "moveFrequency": 3,
      "moveRoute": {
        "list": [
          {
            "code": 0,
            "parameters": []
          }
        ],
        "repeat": True,
        "skippable": False,
        "wait": False
      },
      "moveSpeed": 3,
      "moveType": 0,
      "priorityType": 1,
      "stepAnime": False,
      "through": False,
      "trigger": 0,
      "walkAnime": True
    }
  ],
  "x": 0,
  "y": 0
}


map_dir = './' #os.dirname...

def break_lines(text, line_length = 42):
    words = text.split()
    lines = []
    current_line = ""
    
    for word in words:
        if len(current_line) + len(word) <= line_length:
            current_line += word + " "
        else:
            lines.append(current_line.strip())
            current_line = word + " "
    
    # Add the last line to lines
    if current_line:
        lines.append(current_line.strip())
    
    return lines

def make_dialog(npc_name, people_tile_id, people_id, lines):
    ret = []
    for i in range(0, len(lines)):
        if i%3 == 0:
            ret.append({
                "code": 101,
                "indent": 0,
                "parameters": [
                    "People{}".format(people_tile_id),
                    people_id,
                    0,
                    2,
                    npc_name
                    ]
                })
        ret.append({
            "code": 401,
            "indent": 0,
            "parameters": [
                lines[i]
                ]
            })
    ret.append({
        "code": 0,
        "indent": 0,
        "parameters": []
        })

    return ret

def max_id(events):
    return max([ e['id'] if e is not None else -1 for e in events])

def remove_npc_generated(events, note=NOTE_V0_1):
    return [ e for e in events if e is None or e['note'] != note ]


map_file_path = os.path.join(map_dir, map_name)
map_json = {}
with open(map_file_path, 'r') as map_file:
    create_backup_file(map_file_path, os.path.join(map_dir, 'backups/'))
    map_json = json.load(map_file)
events = map_json['events']
events = remove_npc_generated(events)
map_json['events'] = events
width = map_json['width']
height = map_json['height']
next_id = max_id(events) + 1
continueToMake = True
for dialog in npc_dialogs:
    print(f"making npc {next_id} for {dialog}...")
    npc = copy.deepcopy(NPC_TEMPLATE)
    npc['id'] = next_id
    npc_page = npc['pages'][0]
    npc_page['moveSpeed'] = random.randint(2, 3)
    npc_page['moveType'] = 1 # 0 is fixed 1 is random moving random.randint(0, 1)
    npc_people_tile_id = 1 #random.randint(1, 4) # 1 villager 2 adventuer 3 royal 4 citizen
    npc_people_id = random.randint(0, 7)
    npc_page['image'] = {
                "tileId": 0,
                "characterName": "People{}".format(npc_people_tile_id),
                "direction": 2, # 2 is face down and stand, 6 is right
                "pattern": 1,
                "characterIndex": npc_people_id
            }

    lines = break_lines(dialog)

    npc_page['list'] = make_dialog(
            "Villager",
            npc_people_tile_id,
            npc_people_id,
            lines
            )

    #print(npc_page)
    offset = 5
    npc['x'] = random.randint(offset, width-offset)
    npc['y'] = random.randint(offset, height-offset)

    events.append(npc)
    next_id += 1

with open(map_file_path, 'w') as map_file:
    map_file.write(json.dumps(map_json))
    
