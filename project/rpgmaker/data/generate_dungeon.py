import random
import copy
import json

num_levels = 8
num_depth = 6
max_branches = random.randint(1, 3)
name = 'icytunnel'
battleback1Name = 'IceCave'
battleback2Name = 'IceCave'
bgm = {
    "name": "FF4/12 Into the Darkness",
    "pan": 0,
    "pitch": 100,
    "volume": 90
}
displayName =  ""
RPGMakerID = 999 # starting from 38, will overwrite

with open('./MapInfos.json', 'r') as map_file:
    map_json = json.loads(map_file.read())
    RPGMakerID = max([x['id'] for x in map_json if x is not None]) + 1
    print(f"MaxId is {RPGMakerID}")





def is_diggable(dungeon, y, x, ny, nx):
    width = len(dungeon[0])
    height = len(dungeon)

    if nx < 1 or nx >= width -1 or ny < 1 or ny >= height -1:
        return False

    if dungeon[ny][nx] != '#':
        return False

    neighbors = [
        (nx - 1, ny),  # Left
        (nx + 1, ny),  # Right
        (nx, ny - 1),  # Up
        (nx, ny + 1)   # Down
    ]

    for neighbor in neighbors:
        nx, ny = neighbor
        if nx == x and ny == y:
            continue  # Skip the current tile (x, y)
        if nx >= 1 and nx < width - 1 and ny >= 1 and ny < height -1:
            if dungeon[ny][nx] != '#':
                return False
        else:
            return False

    return True


def generate_dungeon(start_x, start_y, width, height, max_branches, entrance = 'a'):
    if max_branches == 0:
        return 0
    dungeon[start_y][start_x] = entrance

    path = [(start_x, start_y)]
    branches = []
    path_length = 0
    max_path_length = random.randint(width + height, int((width + height)*2.0))
    max_iterations = width * height * 10
    iterations = 0

    while path_length < max_path_length:
        iterations += 1
        if iterations > max_iterations:
            print("Unable to generate a valid dungeon.")
            break

        current_x, current_y = path[-1]
        possible_directions = []

        if current_x > 1 and is_diggable(dungeon, current_y, current_x, current_y, current_x - 1):
            possible_directions.append((-1, 0))
        if current_x < width - 1 and is_diggable(dungeon, current_y, current_x, current_y, current_x + 1):
            possible_directions.append((1, 0))
        if current_y > 1 and is_diggable(dungeon, current_y, current_x, current_y-1, current_x):
            possible_directions.append((0, -1))
        if current_y < height - 1 and is_diggable(dungeon, current_y, current_x, current_y+1, current_x):
            possible_directions.append((0, 1))

        if len(possible_directions) > 0:
            next_direction = random.choice(possible_directions)
            next_x = current_x + next_direction[0]
            next_y = current_y + next_direction[1]
            dungeon[next_y][next_x] = '*'
            path.append((next_x, next_y))
            path_length += 1
        else:
            break

    if path_length > 1:
        dungeon[current_y][current_x] = 'x'

    middle_index = path_length // 2
    for i in range(0, path_length):
        if middle_index == 0:
            continue
        middle_index += i
        middle_index %= path_length
        length = generate_dungeon(path[middle_index][0], path[middle_index][1], width, height, max_branches - 1, '*')
        if length > 1:
            return path_length
    
    return path_length

def shrink_dungeon(dungeon):
    # Find the boundaries of the dungeon
    min_row = float('inf')
    max_row = float('-inf')
    min_col = float('inf')
    max_col = float('-inf')

    for row in range(len(dungeon)):
        for col in range(len(dungeon[row])):
            if dungeon[row][col] != '#':
                min_row = min(min_row, row)
                max_row = max(max_row, row)
                min_col = min(min_col, col)
                max_col = max(max_col, col)

    min_row -= 1
    max_row += 1
    min_col -= 1
    max_col += 1
    # Shrink the dungeon based on the boundaries
    shrunk_dungeon = []
    for row in range(min_row, max_row + 1):
        shrunk_row = dungeon[row][min_col:max_col + 1]
        shrunk_dungeon.append(shrunk_row)

    return shrunk_dungeon

# Generate a dungeon with 2-5 branches
dungeon_grids = []
dungeon_infos = []

for level in range(num_levels):
    width = random.randint(20, 50)
    height = random.randint(20, 30)

    dungeon = [['#' for _ in range(width)] for _ in range(height)]
    start_x = random.randint(1, width - 2)
    start_y = random.randint(1, height - 2)

    generate_dungeon(start_x, start_y, width, height, max_branches)
    dungeon = shrink_dungeon(dungeon)
    dungeon_grids.append(dungeon)
    dungeon_infos.append({
        'depth': -1,
        'i': level,
        "data": dungeon,
        "to":[],
        })

def find_character(grid, character):
    coordinates = []
    for y, row in enumerate(grid):
        for x, value in enumerate(row):
            if value == character:
                coordinates.append((x, y))
    return coordinates

def connect(x, y):
    fromxy = random.choice(find_character(dungeon_grids[x], 'x'))
    toxy = random.choice(find_character(dungeon_grids[y], 'a'))
    dungeon_infos[x]['to'].append({
            'index': y,
            'x': fromxy[0],
            'y': fromxy[1],
            'nx': toxy[0],
            'ny': toxy[1]
            })
    dungeon_infos[y]['from'] = {
            'index': x,
            'x': toxy[0],
            'y': toxy[1],
            'nx': fromxy[0],
            'ny': fromxy[1]
            }
    dungeon_infos[y]['depth'] = dungeon_infos[x]['depth'] + 1
    dungeon_infos[x]['data'][fromxy[1]][fromxy[0]] = 'A'

for d in range(0, num_depth-1):
    dungeon_infos[d]['depth'] = d
    connect(d, d+1)

for r in range(num_depth, num_levels):
    ava = [dun['i'] for dun in dungeon_infos if dun['depth'] < num_depth - 1 and dun['depth'] >= 0 and len(find_character(dun['data'], 'x')) > 0 ]
    if len(ava) == 0:
        dungeon_infos[r]['depth'] = 0
        continue
    di = random.choice(ava)
    connect(di,r)
    


# Print the dungeon grids
for level, dungeon in enumerate(dungeon_grids):
    print(f"Level {level + 1}:")
    for row in dungeon:
        print(''.join(row))
    print()

jsonstr = '''{
"autoplayBgm":true,"autoplayBgs":false,"battleback1Name":"Crystal","battleback2Name":"Crystal","bgm":{"name":"FF4/12 Into the Darkness","pan":0,"pitch":100,"volume":90},"bgs":{"name":"","pan":0,"pitch":100,"volume":90},"disableDashing":false,"displayName":"","encounterList":[],"encounterStep":30,"height":5,"note":"","parallaxLoopX":false,"parallaxLoopY":false,"parallaxName":"DarkSpace","parallaxShow":true,"parallaxSx":0,"parallaxSy":0,"scrollType":0,"specifyBattleback":true,"tilesetId":4,"width":5,
"data":[],
"events":[]
}
'''
TEMPLATE = json.loads(jsonstr)


def flatten_grid(grid):
    flattened_list = [element for row in grid for element in row]
    return flattened_list
def map_dungeon(x):
    if x == '#':
        return 0
    else:
        return 3200 #3248 #3378

ret = []
for level, dungeon_info in enumerate(dungeon_infos):
    rid = dungeon_info['i'] + RPGMakerID
    out = copy.deepcopy(TEMPLATE)
    dungeon = dungeon_info['data']
    width = len(dungeon[0])
    height = len(dungeon)

    out['width'] = width
    out['height'] = height
    out['data'] = [map_dungeon(x) for x in flatten_grid(dungeon)] + [0]*(width*height*5)
    out['bgm'] = bgm
    out['note'] = 'generated v0.1'
    out['battleback1Name'] = battleback1Name
    out['battleback2Name'] = battleback2Name
    out['displayName'] = displayName
    out['events'].append(None)
    if 'from' in dungeon_info and dungeon_info['from']['index'] >= 0:
        fromd = dungeon_info['from']
        out['events'].append(
                {
                    "id": len(out['events']),
                    "name": f"Transfer ({name + str(fromd['index'])})",
                    "note": "generated v0.1",
                    "pages": [
                        {
                            "conditions": {
                                "actorId": 1,
                                "actorValid": False,
                                "itemId": 1,
                                "itemValid": False,
                                "selfSwitchCh": "A",
                                "selfSwitchValid": False,
                                "switch1Id": 1,
                                "switch1Valid": False,
                                "switch2Id": 1,
                                "switch2Valid": False,
                                "variableId": 1,
                                "variableValid": False,
                                "variableValue": 0
                                },
                            "directionFix": False,
                            "image": {
                                "tileId": random.choice([2, 3]),
                                "characterName": "",
                                "characterIndex": 0,
                                "direction": 2,
                                "pattern": 0
                                },
                            "list": [
                                {
                                    "code": 250,
                                    "indent": 0,
                                    "parameters": [
                                        {
                                            "name": "Move1",
                                            "volume": 90,
                                            "pitch": 100,
                                            "pan": 0
                                            }
                                        ]
                                    },
                                {
                                    "code": 201,
                                    "indent": 0,
                                    "parameters": [
                                        0,
                                        fromd['index'] + RPGMakerID,
                                        fromd['nx'],
                                        fromd['ny'],
                                        4,
                                        0
                                        ]
                                    },
                                {
                                    "code": 0,
                                    "indent": 0,
                                    "parameters": []
                                    }
                                ],
          "moveFrequency": 3,
          "moveRoute": {
                  "list": [
                      {
                          "code": 0,
                          "parameters": []
                          }
                      ],
                  "repeat": True,
                  "skippable": False,
                  "wait": False
                  },
          "moveSpeed": 3,
          "moveType": 0,
          "priorityType": 0,
          "stepAnime": False,
          "through": False,
          "trigger": 1,
          "walkAnime": True
        }
      ],
      "x": fromd['x'],
      "y": fromd['y']
        })

    for to in dungeon_info['to']:
        out['events'].append(
                {
                    "id": len(out['events']),
                    "name": f"Transfer ({name + str(to['index'])})",
                    "note": "generated v0.1",
                    "pages": [
                        {
                            "conditions": {
                                "actorId": 1,
                                "actorValid": False,
                                "itemId": 1,
                                "itemValid": False,
                                "selfSwitchCh": "A",
                                "selfSwitchValid": False,
                                "switch1Id": 1,
                                "switch1Valid": False,
                                "switch2Id": 1,
                                "switch2Valid": False,
                                "variableId": 1,
                                "variableValid": False,
                                "variableValue": 0
                                },
                            "directionFix": False,
                            "image": {
                                "tileId": random.choice([10, 11]),
                                "characterName": "",
                                "characterIndex": 0,
                                "direction": 2,
                                "pattern": 0
                                },
                            "list": [
                                {
                                    "code": 250,
                                    "indent": 0,
                                    "parameters": [
                                        {
                                            "name": "Move1",
                                            "volume": 90,
                                            "pitch": 100,
                                            "pan": 0
                                            }
                                        ]
                                    },
                                {
                                    "code": 201,
                                    "indent": 0,
                                    "parameters": [
                                        0,
                                        to['index'] + RPGMakerID,
                                        to['nx'],
                                        to['ny'],
                                        4,
                                        0
                                        ]
                                    },
                                {
                                    "code": 0,
                                    "indent": 0,
                                    "parameters": []
                                    }
                                ],
          "moveFrequency": 3,
          "moveRoute": {
                  "list": [
                      {
                          "code": 0,
                          "parameters": []
                          }
                      ],
                  "repeat": True,
                  "skippable": False,
                  "wait": False
                  },
          "moveSpeed": 3,
          "moveType": 0,
          "priorityType": 0,
          "stepAnime": False,
          "through": False,
          "trigger": 1,
          "walkAnime": True
        }
      ],
      "x": to['x'],
      "y": to['y']
        })
    with open(f"./Map{rid:0>3}.json", 'w') as f:
        f.write(json.dumps(out))
    ret.append({"id":rid,"expanded":False,"name":f"{name + str(dungeon_info['i'])}","order":23,"parentId":1,"scrollX":0,"scrollY":0})

print(f"generated {rid} to {rid + num_levels - 1} and boss is in map {rid + num_depth - 1}")
print(json.dumps(ret))






