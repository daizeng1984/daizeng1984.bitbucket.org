"""
global story:
    Reid lost memory when he's ashore around Woodside. But he still have passion to fight again monsters.
    Reid & his father Lucas one of the 4 hero (Lucas, Olivia, Gabriel, Ava) that defeat malachi but magic stones that powers the ancient technology was destroyed and only shatters.
"""

"""
I'm creating a RPG game, here's the its background story in brief: Terranova is a continent surroundded by oceans. Terranova means new earth. It was named by the first habitat. The people in Terranova is happy! 68 years ago, Malachi came to Terranova and destroyed the cities. That is because Malachi wanted the Magic Orb (which brings prosperity to civilization), but four heroes stood up and deafeated Malachi, but Malachi managed to escape to the Underworld. Now we have a peaceful time.

People start to rebuild their home in this land and our main character Reid just started his journey to defeat Malachi and its followers. He came to a calm small village (called Woodside) and people here is very chill. But recently a dark follower of Malachi called HiMonster started to be active and sent his minions to dig a hole to this village. But villagers know nothing about it.

Reid will come to this village and meet his first friend Eliot, who has also been investigating the recent abormal events for a while.

All NPCs in this village will talk about: recent

1. their daily life
2. something they observed about HiMonster (whom they don't know the name at this moment) and recent monster increasing
3. very subtle and vague information about malachi. All they know so far is: Malachi was defeated 30 years ago by 4 anonymous heros.
4. surprise but welcome to see a young traveler like Reid come to this village

Can you generate 20 NPC dialog based on above information? Format them into python string lists for each NPC. Each string is for one NPC's dialog. Try to make it diverse that it looks real person's talk. Sometime they might also talk to themselves.

Some examples, please don't add any prefix. Just dialog:
npc_dialogs = [
    "Welcome to Woodside, a nice and calm village. Hope you have a good day!",
    "The soil in Terranova used to be very fertile before Malachi."
    ...
]

"""


npc_dialogs = [
    "Welcome to Woodside, a peaceful village. Nice to meet you!",
    "I wake up every morning to the sound of birds chirping. It's so calming.",
    "Have you noticed an increase in monster activity recently? It's quite unusual.",
    "I saw a strange figure lurking around the outskirts of the village. Gives me the creeps.",
    "The crops have been growing well this season. The soil in Terranova is truly fertile.",
    "I heard rumors about a dark presence looming over our land. It's unsettling, to say the least.",
    "Sometimes, I wonder what lies beyond the ocean that surrounds us. Endless mysteries.",
    "Did you see those footprints near the forest? They seem larger than any creature I've seen before.",
    "Every night, I light a candle in remembrance of the heroes who defeated Malachi long ago.",
    "Ah, to be young and embark on a grand adventure. Your arrival brings a sense of hope to our village.",
    "I've been practicing my swordsmanship lately. We need to be prepared for any dangers that may arise.",
    "The children of Woodside are full of curiosity. They ask me about the heroes who defeated Malachi.",
    "I found an old journal mentioning the Magic Orb. Its power must be immense if Malachi desired it so much.",
    "A cold breeze brushes my cheek sometimes. It's as if the spirits of the fallen heroes still watch over us.",
    "I don't know much about goblins, but their presence feels ominous. Something is not right.",
    "The village blacksmith has been working tirelessly to forge stronger weapons.",
    "I dreamt of a hidden passage leading to the Underworld. Could it be a sign of impending danger?",
    "The recent moonlit nights have been mesmerizing. It's like the sky is filled with a thousand stories.",
    "Malachi's defeat brought peace, but we mustn't forget the sacrifices made by the anonymous heroes.",
    "I overheard whispers of ancient relics hidden deep within Terranova. They could hold great power.",
    "Seeing a young traveler like you reminds me of the hope and determination that burns within all of us.",
    "If I see goblins, I'd run away as fast as I can! They usually come in two. You'll never want to fight them alone.",
    "The blue bird is just annoying for travelers... Did you see them along your way here?",
    "Malachi burnt half of Terranova into desert when he came here 68 years ago. How do I know huh? I read from library!",
    "Eliot is a great young man. He is kind and know Holy magic. He cures and heals our wounds.",
    "4 years ago, I was the one who built all the fences. However, I saw a hole somewhere now. I need to fix it now!",
    "I hate goblins, they are dangerous.",
    "The abondon house is called haunted house. I guess it's fine. It's just scary for children.",
    "I saw a abnormal big tree in our village. But if you look closely it looks like multiple trees.",
    "I don't want to fight. I just want to sleep and lay down...",
    "The man who live in the house at north east corner is very rude. I don't think everyone like him.",
    "There is a transporter to Crystal Village. I don't know how QuickSilver broke it.",
    "I grows vegetables myself. Do you grow them? I like to watch them grow.",
    "I'm not good at growing vegetables. One day I forgot to water them and they died...",
    "How did you come here? By boat? I didn't see any ships and boats. Oh, by airship? I've seen airship in my life. That's an old technology before Malachi.",
    "I like running in this village every morning.",
    "I feel I'm hungry. I want to eat some burgers and fries. Oh, I'm now even more hungry by thinking it...I better eat right now!",
    "I see street lights in this village, is it strange? It's not a city!",
    "There is no overseer in our village. But everyone just follow rules. That's why our village is calm and people are chill.",
]


"""

I'm creating a RPG game, here's the its background story in brief: Terranova is a continent surroundded by oceans. Terranova means new earth. It was named by the first habitat. The people in Terranova is happy! 68 years ago, Malachi came to Terranova and destroyed the cities. That is because Malachi wanted the Magic Orb (which brings prosperity to civilization), but four heroes stood up and deafeated Malachi, but Malachi managed to escape to the Underworld. Now we have a peaceful time.

People start to rebuild their home in this land and our main character Reid just started his journey to defeat Malachi and its followers. After Reid and his friend Elliot, met in [Woodside Village] defeating HiMonster, got a boat, they came to a trading hub coastal village (called [Crystal Village]) and people here used to do trading oversea before Malachi came and destoryed it. Now it's still a small trading hub for some goods. It doesn't have agriculture and mainly relies on a big Village from continent by delivery business though the coninent cave tunnel called Crystal Tunnel. But recently a dark dragon called "QuickSilver" started to occupy the tunnel and crystalized the tunnel make it its lair. Villagers have trouble to get goods and fresh crops from continent and this make their life a lot of troubles. Village really want a her to fight and defeat this dragon. A young woman magician Kasey came here and swore to help out these village to defeat the dragon. So she went to the tunnel but she never came back since left. 

All NPCs in this village will talk about:

1. their daily life
2. something they observed about QuickSilver
3. Kasey
4. history of village
5. how trade hub shape the village


Can you generate 40 NPC dialog based on above information? Format them into python string lists for each NPC. Each string is for one NPC's dialog. Try to make it diverse that it looks real person's talk. Sometime they might also talk to themselves.

Some examples, please don't add any prefix. Just dialog:
npc_dialogs = [
    "The soil in Terranova used to be very fertile before Malachi."
    ...
]
"""

npc_dialogs = [
    "Welcome to Crystal Village, the heart of trade in Terranova. Enjoy your stay!",
    "Our days revolve around the bustling trade activities. It's always exciting to see new goods arriving.",
    "I remember the time when QuickSilver didn't exist. We had free access to the Crystal Tunnel without any worries.",
    "The Crystal Tunnel used to shine so beautifully, but now it's covered in the dragon's dark icy crystals.",
    "In the past, Crystal Village was known for its thriving trading hub overseas. It's a shame we lost that after Malachi's attack.",
    "Kasey is such a brave magician who ventured into the Crystal Tunnel to help us. I hope she's safe.",
    "Every day, I pray for Kasey's success in defeating QuickSilver and restoring our access to the Crystal Tunnel.",
    "QuickSilver's presence has made trading difficult. We're running out of essential supplies from the continent.",
    "I used to travel to the continent through the Crystal Tunnel. Now, it's too dangerous to even approach it.",
    "The dragon's silver scratch will crystalize everything it touches. It's a chilling sight to behold.",
    "We rely heavily on the continental village for our deliveries. Without their shipments, our supplies dwindle.",
    "Kasey's bravery has given us hope. We eagerly await her triumphant return from the Crystal Tunnel.",
    "My friend traveled throught Crystal Tunnel month ago. I hope she is safe.",
    "Trade is the lifeblood of this village. It brings us not just goods, but also cultural exchange and diversity.",
    "Our delivery business is struggling now because of the damn QuickSilver!",
    "I hope Kasey is safe... I'm so worried...",
    "In the old days, Crystal Village was a bustling hub where traders from all corners of Terranova gathered.",
    "QuickSilver's lair in the Crystal Tunnel feels like an impenetrable fortress. It's a daunting challenge.",
    "We used to receive exotic spices, fabrics, and treasures from the continent. Oh, how I miss those days.",
    "I fear the day when our supplies run dry. We must find a way to reclaim the Crystal Tunnel.",
    "Kasey's disappearance weighs heavily on our hearts. We must not lose hope in her abilities.",
    "Trade has shaped our village's identity. We've learned to adapt and thrive despite the challenges we face.",
    "Since Malachi's attack, our coastlines are full of rocks and crystals. Ship can no longer run in this place.",
    "Before Malachi's attack, Crystal Village was a beacon of commerce and prosperity. We'll rebuild it once again.",
    "Did you travel from Woodside? You know, there used to have a transporter available before QuickSilver.",
    "Have you ever seen an ancient vehicle running? I read from book that there are some magical 4 wheels vehicle can transport people from one place to another. But Malachi destroyed all the magical powered technologies.",
    "Could QuickSilver be related to Malachi?",
    "There are a great library in Woodside. Go check it out if you want to read about the history of Terranova.",
    "Can you fight, travelers? If so, please help us fight QuickSilver!",
    "In the village's history, the Crystal Tunnel played a pivotal role in our growth and prosperity.",
    "Crystal Village was an old city remain today. Is it interesting?",
    "I remember when the Crystal Tunnel was vibrant, echoing with the laughter of traders. It feels like a dream now.",
    "Some folks told me Crystal Tunnel is filled with monsters and they are all weak to fire. If you have a fire magician, it'll be handy.",
    "Watch out the Crystal Bat. They are very dangerous.",
    "You seem to need key to access the lair of QuickSilver. Kasey went Crystal Tunnel and had been searching for it for a few days now.",
    "Buy supplies before you go to the tunnel, some antidote, potion and magic water too. It's not advertisement. It's suggestion!",
    "I'll have to relocate to Woodside if it keeps like this!",
    "Someone said they saw Malachi and his followers coming back! I hope it's not true.",
    "If I'm a fire magic swordman, I'll be fighting these monsters!",
    "You have a blazing sword?! That'll be handy in Crystal Tunnel. But most monsters are in hard shell. It's a bit hard to hurt them.",
]




"""

I'm creating a RPG game, here's the its background story in brief: Terranova is a continent surroundded by oceans. Terranova means new earth. It was named by the first habitat. The people in Terranova is happy! 68 years ago, Malachi came to Terranova and destroyed the cities. That is because Malachi wanted the Magic Orb (which brings prosperity to civilization), but four heroes stood up and deafeated Malachi, but Malachi managed to escape to the Underworld. Now we have a peaceful time.

People start to rebuild their home in this land and our main character Reid just started his journey to defeat Malachi and its followers. After Reid and his friend Elliot, met in [Woodside Village] defeating HiMonster, got a boat, they came to a trading hub coastal village (called [Crystal Village]) and people here used to do trading oversea before Malachi came and destoryed it. Now it's still a small trading hub for some goods. It doesn't have agriculture and mainly relies on a big Village named Haveston from continent by delivery business though the coninent cave tunnel called Crystal Tunnel. But recently a dark dragon called "QuickSilver" started to occupy the tunnel and crystalized the tunnel make it its lair. Villagers have trouble to get goods and fresh crops from continent and this make their life a lot of troubles. Reid & Eliot came here and helped these village and saved a magician adventuer named Kasey and together they defeated the dragon and cleared the tunnel. Then they went to Haveston.

All NPCs in this village will talk about:

* their agricultural daily life
* something they observed about QuickSilver
* history of village
* Malachi's rumor

In their dialog I want you to expose some clues:
* (Key) Someone saw Malachi and his followers are coming back. Hi Monster and Malachi preempt hero 
* 

Can you generate 40 NPC dialog based on above information? Format them into python string lists for each NPC. Each string is for one NPC's dialog. Try to make it diverse that it looks real person's talk. Sometime they might also talk to themselves.

Some examples, please don't add any prefix. Just dialog:
npc_dialogs = [
    "The soil in Terranova used to be very fertile before Malachi."
    ...
]
"""

"""
Now you are a dungeon generator. 
- The dungeon is a list of grids like levels. Each level is connected to another one by transfer point so that player and enter one dungeon level and exit from another. 
- level is represented using 2D grid of '*' and '#' where '*' means empty space that player can walk, while '#' means wall that block player. '+' means treasure and it can also block player to walk. '-' means miniboss enemies that user has to fight to unblock. 
- '$' is the final boss of this dungeon and in the deepest level and can be unblocked if defeated. *Only one final boss* in the whole dungeon and it must be in the final level.
- We also use alphabet 'a' to 'z' to represent transfer point from one dungeon level to another level. Same alphabet transfer to the same alphabet on another dungeon. 'a' would usually means the entrance of the whole dungeon. 
- Transfer pointer *only comes in pairs* to connect two levels in a dungeon except entrance 'a' and exit which only have one.
- width/height of each dungeon level should be random in range of 20 - 80, and width height are not the same. The more random, the more fun!
- empty space is also called path. Like in maze, path is usually 1 or 2 block wide e.q. there should not has any 2x2 grids made of only '*' unless it's final boss lair. 
- like maze each level should have some dead end with at the end treasure.
- dungeon is all connected, you can walk from 'a' to exit if you can defeat the miniboss '-' and final boss '$'.

Here's one example of the dungeon of level 3:

# dungeon from a to d
dungeon_grids = [ 
'''
###################
#a*************+###
#####*#############
#+*************####
##############*####
##***********-*####
##b################
''',

'''
########################
#b***#####***###*****c##
####*##****+**##*#######
#+##*##*#####******-+###
#****##*################
####****************####
########################
''',

'''
######################
#***#************+####
#*#*#*################
#*#*#**********#######
#*#*###*######*#######
#*#*###*######*****$##
#+#*###*###########*##
###*###*###########*d#
#c****************+###
#################***+#
######################
''',
]


Please generate a dungeon of 8 levels and make it fun! Don't use my design, be creative. Don't write me a code, manually layout the design as my example.

"""
I'm creating a RPG game, here's the its background story in brief: Terranova is a continent surroundded by oceans. Terranova means new earth. It was named by the first habitat. The people in Terranova is happy! 68 years ago, Malachi came to Terranova and destroyed the cities. That is because Malachi wanted the Magic Orb (which brings prosperity to civilization), but four heroes stood up and deafeated Malachi, but Malachi managed to escape to the Underworld. Now we have a peaceful time.

People start to rebuild their home in this land and our main character Reid just started his journey to defeat Malachi and its followers. After Reid and his friend Elliot, met in [Woodside Village] defeating HiMonster, got a boat, they came to a trading hub coastal village (called [Crystal Village]) and people here used to do trading oversea before Malachi came and destoryed it. Now it's still a small trading hub for some goods. It doesn't have agriculture and mainly relies on a big Village named Haveston from continent by delivery business though the coninent cave tunnel called Crystal Tunnel. But recently a dark dragon called "QuickSilver" started to occupy the tunnel and crystalized the tunnel make it its lair. Villagers have trouble to get goods and fresh crops from continent and this make their life a lot of troubles. Reid & Eliot came here and helped these village and saved a magician adventuer named Kasey and together they defeated the dragon and cleared the tunnel. Then they went to Haveston, the famous agriculture village. The haveston now has new trouble that nearby forest is contaminated by evil purple venom, released by a treeant creature named Woodstock. Now the pollution start to corrupt everything in that forest and maybe the soil will turn back for haveston. The three survive from Malachi's attack, saved by 2 of the 4 Hero (Gabriel and Ava) and is now taken to Haveston Inn. When they wake up. They met Alex, a martial arts young man. Together, they decide to hang out in Haveston and talk to people there.

All NPCs in this village will talk about:

* their agricultural daily life
* something they observed about QuickSilver before, but now it's gone because of the 3
* history of village
* Gabriel and Ava
* Malachi

In their dialog I want you to expose some clues:
* (Key) Someone saw Malachi and his followers are coming back.
* (Key) Gabriel and Ava saved the Malachi attack and saved the 3 young hero
* (Key) three young hero help solved the problem of Crystal Tunnel's QuickSilver

Can you generate 40 NPC dialog based on above information? Format them into python string lists for each NPC. Each string is for one NPC's dialog. Try to make it diverse that it looks real person's talk. Sometime they might also talk to themselves.

Some examples, please don't add any prefix. Just dialog:
npc_dialogs = [
    "The soil in Terranova used to be very fertile before Malachi."
    ...
]
"""



npc_dialogs = [
    "You know, life in Terranova wasn't always this peaceful. We've come a long way since Malachi's attack.",
    "I used to be a trader before Malachi arrived. Now, I'm focused on rebuilding and cultivating the land.",
    "The Magic Orb... Some say it's just a legend, but I believe it holds the key to our civilization's prosperity.",
    "Wow, I heard Gabriel and Ava saved you guys from Malachi! I wish I could have their strenth.",
    "Crystal Village, once a bustling trading hub, reduced to ashes by Malachi. We're slowly rebuilding.",
    "Have you seen the crystalized tunnel? It's like something out of a nightmare. I'm glad it's cleared now.",
    "The Crystal Tunnel was our lifeline, connecting us to Haveston. QuickSilver's presence was a nightmare.",
    "You saved us from QuickSilver! and our business would be resumed now!",
    "I used to travel to Haveston for business. It breaks my heart to see the forest corruption affecting them.",
    "Woodstock and its evil purple venom... It's devouring everything in its path. We must find a way to stop it.",
    "Malachi's escape haunts us to this day and he's coming back. I hope we 4 heroes will once again fight against it.",
    "Ah, Haveston, the lush agricultural village. The contaminated forest threatens their way of life.",
    "The polluted forest is an omen of doom. If we don't act soon, Haveston might crumble.",
    "I've heard rumors of Malachi's return. We must prepare ourselves for the dark days ahead.",
    "Reid, Elliot, and Kasey, the unlikely heroes that saved Crystal Village from QuickSilver.",
    "Did you know Crystal Village was once famous for its vibrant markets? Malachi took that all away.",
    "The crystalized tunnel was like a prison. QuickSilver's grip suffocated our dreams of prosperity.",
    "I miss the days when the Crystal Tunnel bustled with traders from all walks of life. It was a sight to behold.",
    "Malachi's shadow still lingers in our minds. We must stay vigilant and never let our guard down.",
    "Reid and Elliot's courage restored our hope. They are the beacons of light in this dark world.",
    "Ah, the Crystal Tunnel, now freed from QuickSilver's clutches. It's a symbol of our triumph over darkness.",
    "Our lives revolve around Haveston's bountiful harvests. The forest corruption threatens us all.",
    "The soil in Terranova used to be very fertile before Malachi. We've been working hard to restore it.",
    "Gabriel and Ava, the saviors of Terranova. Their tale is etched in our hearts forever.",
    "Reid and Elliot's journey started here in Crystal Village. It's amazing how far they've come.",
    "The Crystal Tunnel was once a lifeline, now tainted by QuickSilver's presence. We're grateful for its liberation.",
    "Before Malachi's attack, our village thrived on trade. It's been a struggle to rebuild ever since.",
    "Gabriel and Ava risked their lives to save us all. We owe them a debt we can never repay.",
    "Reid and Elliot's arrival brought a spark of hope to our village. They're destined for greatness.",
    "The Crystal Tunnel's restoration is a testament to the resilience and resourcefulness of our people.",
    "We used to rely on trade with Haveston for our daily needs. It's been a struggle without it.",
    "The contaminated forest threatens Haveston's prosperity. We must find a solution before it's too late.",
    "Malachi's return? I thought we were finally free from his tyranny. We must prepare for the worst.",
    "Reid, Elliot, and Kasey, the heroes who faced QuickSilver. Their valor will be remembered for generations.",
    "Crystal Village's history is intertwined with the Crystal Tunnel. We'll never forget the darkness it endured.",
    "Gabriel and Ava's bravery saved countless lives. We're forever indebted to them.",
    "Reid, Elliot, and Kasey... They're the embodiment of courage. Crystal Village is lucky to have them.",
    "The Crystal Tunnel's liberation from QuickSilver's grip has brought newfound hope to our village.",
    "The pollution in the nearby forest threatens Haveston's prosperity. We must find a solution soon.",
]

