#!/bin/bash
# use below code... to get the stupid output from rpgmaker
# convert ava-young.png -gravity northwest -crop 25%x50%+0+0 +repage ava-young.png
#
for file in *.png; do
    convert "$file" -gravity northwest -crop 25%x50%+0+0 +repage "${file%.png}_cropped.png"
    done

montage -geometry +0+0 -background none -tile 4x2 *_cropped.png final_image.png
 

# Clean up intermediate files
rm *_cropped.png

echo "Images cropped and concatenated successfully."



