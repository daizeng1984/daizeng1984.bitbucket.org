#!/bin/bash
# Resize all PNG files in the current directory to 144
for file in *.png; do
    convert "$file" -resize 144x144\> "${file%.png}_resized.png"
    done
# Concatenate resized PNGs into a 2x4 tiled image
montage -geometry +0+0 -background none -tile 4x2 *_resized.png final_image.png
 

# Clean up intermediate files
rm *_resized.png

echo "Images resized and concatenated successfully."
