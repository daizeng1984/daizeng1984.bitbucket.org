rg -l '\\N\[1\]' data/Map*.json | xargs sed -E -i 's/\["Actor1",0,0,2/\["genai\/face1",2,0,2/g'

rg -l '\\N\[6\]' data/Map*.json | xargs sed -E -i 's/\["Actor1",5,0,2/\["genai\/face1",5,0,2/g'
rg -l '\\N\[7\]' data/Map*.json | xargs sed -E -i 's/\["Actor1",6,0,2/\["genai\/face1",3,0,2/g'
rg -l '\\N\[9\]' data/Map*.json | xargs sed -E -i 's/\["Actor1",2,0,2/\["genai\/face1",1,0,2/g'

# gabriel
rg -l 'Gabriel' data/Map*.json | xargs sed -i 's/Gabriel/\\\\N[10]/g'
rg -l 'Lucas' data/Map*.json | xargs sed -i 's/Lucas/\\\\N[17]/g'
rg -l 'Ava' data/Map*.json | xargs sed -i 's/Lucas/\\\\N[18]/g'

rg -l '\\N\[10\]' data/Map*.json | xargs sed -E -i 's/\["Actor3",6,0,2/\["genai\/face2",2,0,2/g'
rg -l '\\N\[11\]' data/Map*.json | xargs sed -E -i 's/\["Actor1",4,0,2/\["genai\/face2",4,0,2/g'
rg -l '\\N\[17\]' data/Map*.json | xargs sed -E -i 's/\["Actor3",0,0,2/\["genai\/face2",5,0,2/g'
rg -l '\\N\[18\]' data/Map*.json | xargs sed -E -i 's/\["Actor3",7,0,2/\["genai\/face2",6,0,2/g'


rg 'woodstock' -i -g **/Map*.json | xargs sed -E -i 's/Woodstock/\\\\N\[182\]/g'
