from PIL import Image

def process_image(input_path, output_path, constant_value):
    # Open the image
    img = Image.open(input_path)
    # Convert the image to RGBA mode (if not already in that mode)
    img = img.convert('RGBA')
    intImg = img.convert('L')

    # Extract the red channel
    r_channel = img.split()[0]

    img.putalpha(intImg // 2)

    # Save the processed image
    img.save(output_path)

# Example usage
input_image_path = '../img/weather/Fog_01a.png'
output_image_path = '../img/weather/Fog_01d.png'
constant_rgb_value = (118, 30, 255)  # Replace with the desired constant RGB values

process_image(input_image_path, output_image_path, constant_rgb_value)


