import sys
import json
from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout,
                             QPushButton, QTextEdit, QFileDialog,
                             QMessageBox, QFormLayout, QLabel, QLineEdit,
                             QScrollArea, QGroupBox)

class JsonEditor(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("JSON Text Editor")

        self.current_file = None
        self.data = {"text": "hello", "text2": "world"}  # Store the loaded JSON data
        self.items_per_page = 20
        self.current_page = 0

        self.form_layout = QFormLayout()
        self.form_group_box = QGroupBox("Key-Value Pairs")
        self.form_group_box.setLayout(self.form_layout)

        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.form_group_box)

        self.open_button = QPushButton("Open JSON")
        self.save_button = QPushButton("Save JSON")
        self.prev_button = QPushButton("Previous Page")
        self.next_button = QPushButton("Next Page")

        self.open_button.clicked.connect(self.open_json)
        self.save_button.clicked.connect(self.save_json)
        self.prev_button.clicked.connect(self.prev_page)
        self.next_button.clicked.connect(self.next_page)

        layout = QVBoxLayout()
        layout.addWidget(self.scroll_area)
        layout.addWidget(self.open_button)
        layout.addWidget(self.save_button)
        layout.addWidget(self.prev_button)
        layout.addWidget(self.next_button)
        self.setLayout(layout)

        self.update_form()
        

    def update_form(self):
        # Clear the existing form layout
        while self.form_layout.count():
            item = self.form_layout.itemAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()
            self.form_layout.removeItem(item)

        start = self.current_page * self.items_per_page
        end = min((self.current_page + 1) * self.items_per_page, len(self.data))

        for i, (key, value) in enumerate(list(self.data.items())[start:end]):
            label = QLabel(key)
            line_edit = QLineEdit(value)
            line_edit.textChanged.connect(lambda text, k=key: self.update_data(k, text))
            self.form_layout.addRow(label, line_edit)

    def update_data(self, key, text):
        self.data[key] = text

    def open_json(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "Open JSON File", "", "JSON Files (*.json);;All Files (*)", options=options)
        if file_name:
            try:
                with open(file_name, 'r', encoding='utf-8') as f:
                    self.data = json.load(f)
                    self.current_file = file_name
                    self.current_page = 0  # Reset page on file open
                    self.update_form()
            except json.JSONDecodeError:
                QMessageBox.critical(self, "Error", "Invalid JSON format.")
            except FileNotFoundError:
                QMessageBox.critical(self, "Error", "File not found.")
            except Exception as e:
                QMessageBox.critical(self, "Error", f"An error occurred: {e}")

    def save_json(self):
        if self.current_file:
            try:
                with open(self.current_file, 'w', encoding='utf-8') as f:
                    json.dump(self.data, f, indent=4, ensure_ascii=False)
                QMessageBox.information(self, "Success", "JSON saved successfully.")
            except Exception as e:
                QMessageBox.critical(self, "Error", f"An error occurred while saving: {e}")
        else:
            QMessageBox.warning(self, "Warning", "No file opened to save.")

    def prev_page(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.update_form()

    def next_page(self):
        if (self.current_page + 1) * self.items_per_page < len(self.data):
            self.current_page += 1
            self.update_form()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    editor = JsonEditor()
    editor.show()
    sys.exit(app.exec_())
