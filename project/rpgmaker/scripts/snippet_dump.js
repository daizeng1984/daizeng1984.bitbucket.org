const id = 'zoomblur';
const filterType = 'zoomblur';
const targetType = Filter_Controller.targetType.FullScreenWithWindow;
const posRefTargetId = -1; //player

$gameMap.createFilter(id, filterType, targetType, [], posRefTargetId);

$gameMap.enableFilter(id, true);

let intense = 1.0;

const intervalId = setInterval(()=>{

    $gameMap.setFilter(id, [0, 0, 0, intense]);
    SceneManager._scene._messageWindow.alpha = 1.0 - intense

    intense -= 0.01;

    if(intense <= 0.0) {
        clearInterval(intervalId);
        $gameMap.eraseFilter(id);
        SceneManager._scene._messageWindow.alpha = 1;
    }

}, 30);



setTimeout(()=>{
    if (this.isOnCurrentMap() && this._eventId > 0) {
        const key = [this._mapId, this._eventId, 'A']
        $gameSelfSwitches.setValue(key, true); //selfswitch A = 0
    }
}, 5000);



const findTreasure = ()=> {
    for(const ev of $gameMap._events) {
        if(!ev || !ev.hasOwnProperty('_pageIndex')) {
            continue;
        }
        const index = ev._pageIndex;
        for(const page of ev.event().pages) {
            try {
                if(page.image && page.image.characterName === "!Chest") {
                    if(page.list) {
                        for(let i = 0; i < page.list.length; ++i) {
                            const l = page.list[i];
                            if(l.code === 250 && l.parameters[0] && l.parameters[0].name.includes('Chest')) {
                                if(index <= i) {
                                    const dist = Math.abs(ev._x - $gamePlayer._x) + Math.abs(ev._y - $gamePlayer._y);
                                    $gameMessage.add(`One chest is at ${dist} steps!`);
                                }
                            }
                        }
                    }
                }
            }
            catch(e) {
                console.log(`Finding Chest encounter issues: ${e}`);
            }
        }
    }

};

findTreasure();


const subject = BattleManager._subject;
const action = BattleManager._action;
console.log(`>>>>>>>>>>> ${subject}, ${action}`)

const actorId = $gameTemp.lastSubjectActorId();
let subject = null;
if(actorId > 0) {
    subject = $gameActors.actor(1);
}

if(subject!==null) {
const index = subject.index()
const battleSpriteSet = SceneManager._scene.children[0];
const newEnemy = new Game_Enemy(+monster_ball, (battleSpriteSet._actorSprites[index]._homeX + 60), battleSpriteSet._actorSprites[index]._homeY - 2 )
$gameTroop._enemies.push(newEnemy)
battleSpriteSet.addEnemy(newEnemy)
newEnemy.requestEffect('flop')
newEnemy.setAlly(true)
// just for record
currentAction.subject().summon(newEnemy)
GameInfoHelper.battleLog(`${newEnemy.enemy().name} is summoned from the Monster Ball!`);
}




const intervalId = setInterval(function() {
    const actor = $gameParty.getActorById(1);
    if (actor.isStateAffected(164)) {
        $gameTemp.requestAnimation([actor], 142);
    } else {
        clearInterval(intervalId); // Stop the interval when the variable is false
    }
}, 1000);

