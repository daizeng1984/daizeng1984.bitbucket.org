const DUMMY_BATTLER = new Game_Battler();
const queueActions = (target, action) => {
    DUMMY_BATTLER.addAction(action);
    const index = BattleManager._actionBattlers.indexOf(DUMMY_BATTLER);
    if(index < 0) {
        BattleManager._actionBattlers.unshift(DUMMY_BATTLER);
    }
};

const showHpDamageAndClearResult = (target, damageSound = true) => {
    BattleManager._logWindow.popupDamage(target);
    if(damageSound) {
        BattleManager._logWindow.displayDamage(target);
    }
    BattleManager._logWindow.displayAffectedStatus(target);
    target._sprite.setupDamagePopup();
    target._result.clear();
};

class ActionOverride {
    static sameSide(actor) {
        if(actor.constructor.name === "Game_Actor") {
            return $gameParty.allBattleMembers();
        }
        else {
            return $gameTroop._enemies;
        }
    }
    
    static otherSide(actor) {
        if(actor.constructor.name === "Game_Actor") {
            return $gameTroop._enemies;
        }
        else {
            return $gameParty.allBattleMembers();
        }
    }
    static checkAndOverrideOnActionStart(subject, currentAction, targets) {
        // target hasn't been finalized yet
        // subject + subject state + action
        let shouldOverride = false;
        const subjectStates = subject.states();
        const subjectStateIds = subject._states;
        // const targetStates = target.states();
        // const targetStateIds = target._states;
        const skill = currentAction.item();

        const allBattleMembers = $gameParty.allBattleMembers();
        const allEnemies = $gameTroop._enemies;
        
        const redirect = GameInfoHelper.getNoteObject(skill, "monster_ball");
        

        return shouldOverride;
    }

    static checkAndOverrideSingleAction(subject, target, currentAction) {
        let ret = false;
        // special actions when known:
        // subject + single target state + action
        const subjectStates = subject.states();
        const subjectStateIds = subject._states;
        const targetStates = target.states();
        const targetStateIds = target._states;
        const skill = currentAction.item();

        const allBattleMembers = $gameParty.allBattleMembers();
        const allEnemies = $gameTroop._enemies;

        const monster_ball = GameInfoHelper.getNoteObject(skill, "monster_ball");
        if(monster_ball && currentAction.subject().isActor()) {
            const newEnemy = $gameParty.summonEnemy(currentAction.subject(), +monster_ball);
            // just for record
            currentAction.subject().summon(newEnemy)
            GameInfoHelper.battleLog(`${newEnemy.enemy().name} is summoned from the Monster Ball!`);
            target.result().success = true;
            ret = true;
        }

        const script_skill = GameInfoHelper.getNoteObject(skill, "script_skill");
        if(script_skill) {
            const skill_name = script_skill.name;
            if(skill_name == 'phantom') {
                if((!(target instanceof Game_Enemy)) || target.isPhantom()) {
                    GameInfoHelper.battleLog(`${subject.name()} failed to perform ${skill.name}!`);
                    return;
                }
                for(const e of $gameTroop._enemies) {
                    if(e.isPhantom() && e.getPhantomTarget() === target) {
                        GameInfoHelper.battleLog(`${target.name()} already has been performed ${skill.name}!`);
                        return;
                    }
                }
                const index = target.index();
                const enemyId = $gameTroop._enemies[index].enemyId();
                const enemySprite = target.getBattleSprite();
                const battleSpriteSet = SceneManager._scene.children[0];
                
                const newEnemy = new Game_Enemy(enemyId, enemySprite._homeX, enemySprite._homeY - 50 )
                $gameTroop._enemies.push(newEnemy)
                const newEnemySprite = battleSpriteSet.addEnemy(newEnemy)
                newEnemy.setPhantom(target);
                
                newEnemySprite.update = function() {
                    Sprite_Enemy.prototype.update.call(this);
                    this.opacity = Math.min(200, this.opacity);
                };
                newEnemySprite.setBlendColor = function(color) {
                    Sprite_Enemy.prototype.setBlendColor.call(this, [Math.max(80, color[0]), Math.max(120, color[1]), Math.max(50, color[2]), Math.max(200, color[3])]);
                }
                newEnemy.isStateResist = function(stateId) {
                    return stateId != this.deathStateId();
                };
                
                newEnemySprite.setBlendColor([0,0,0,0]);
                // newEnemySprite.setBlendColor([90, 80, 40, 200]);
                // newEnemySprite.blendMode = 0;
                // newEnemySprite.opacity = 200;
                GameInfoHelper.battleLog(`${newEnemy.name()} appears!`);
                target.result().success = true;
                ret = true;
            }

        }
        

        // First state check point
        for (let i in targetStateIds) {
            const targetStateId = targetStateIds[i];
            const targetState = targetStates[i];

            const ignoreAction = GameInfoHelper.getNoteObject(targetState, "ignore_action");
            if(ignoreAction) {
                return true;
            }
            
            // if delay skill
            const delaySkillObject = GameInfoHelper.getNoteObject(targetState, "delaySkill");

            if(delaySkillObject) {
                if(!currentAction.isDelayAction() && currentAction.isSkill() && !currentAction.isForUser()) {
                    const newAction = new Game_Action(subject, true);
                    newAction.setSkill(currentAction.item().id);
                    newAction.forceTargetUnit([target]);
                    const delayTurns = delaySkillObject.delayTurns;
                    const result = currentAction.apply(target, true);
                    if(result.hpDamage > 0) {
                        BattleManager.addDelayAction(newAction, delayTurns);
                        target.removeState(targetStateId);
                        BattleManager._logWindow.push("addText", `${subject.name()}'s ${currentAction.item().name} to ${target.name()} gets delayed by ${delayTurns} turns` );
                        BattleManager._logWindow.showAnimation(subject, [target], 138);
                        return true;
                        // we don't need to execute any skill now
                    }
                }
            }
        }

        // if ignore all actions
        
        

        let original_action_applied = false;
        // TODO: think about a general framework for these states + action rules. Figure it's not that obvious. Now we just take it case by case
        const side_effects = GameInfoHelper.getNoteObject(skill, "side_effects");
        if (side_effects) {
            for(let side_effect of side_effects) {
                if(side_effect.type === "applyState") {
                    const applyState = side_effect.state;

                    if(side_effect.hasOwnProperty('on_condition')) {
                        const condition_script = (new Function('return ' + side_effect.on_condition))();
                        if(!condition_script(target)) {
                            continue;
                        }
                    }
                    
                    let actors = []
                    //TODO: to support more flexible scope. this need refactoring
                    // use an example Skill to get scope Game_Action.prototype.itemTargetCandidates
                    if(side_effect.scope === "enemy") {
                        actors = currentAction.opponentsUnit().aliveMembers();
                    }
                    else if (side_effect.scope === "ally") {
                        actors = currentAction.friendsUnit().aliveMembers();
                    }
                    else if (side_effect.scope === "user") {
                        actors = [subject]
                    }
                    else if (side_effect.scope === "target") {
                        actors = [target]
                    }
                    const fa = actors.filter(a => {
                        const filtertarget = side_effect.include_target || !GameInfoHelper.isSameActor(a, target);
                        const filtersubject = side_effect.include_subject || !GameInfoHelper.isSameActor(a, subject);
                        return filtertarget && filtersubject;
                    });

                    if(side_effect.apply_action_before_side_effect && !original_action_applied) {
                        // TODO: change all to invokeAction
                        BattleManager.invokeAction(subject, target, false);
                        original_action_applied = true;
                        // label it as overridded
                        ret = true;
                    }

                    for(let other of fa) {
                        if(Math.random() <= side_effect.probability) {
                            other.addState(applyState);
                        }
                    }
                }
                else if (side_effect.type === "changeState") {
                    let changeMap = {}
                    let foundInMap = false;
                    let ids = targetStateIds;
                    if(side_effect.scope === "user") {
                        ids = subjectStateIds;
                    }
                    
                    for(let stateId of ids) {
                        if(side_effect.map.hasOwnProperty(String(stateId))) {
                            foundInMap = true;
                            const newStateId = side_effect.map[String(stateId)];
                            changeMap[stateId] = newStateId;
                        }
                    }
                    if (!foundInMap) {
                        // reserved for no state 0
                        if(side_effect.map.hasOwnProperty(String(0))) {
                            changeMap[0] = side_effect.map[String(0)]
                        }
                    }
                    if(side_effect.apply_action_before_side_effect && !original_action_applied) {
                        if(Object.keys(changeMap).length > 0) {
                            BattleManager.invokeAction(subject, target, false);
                            // label it as overridded
                            original_action_applied = true;
                            ret = true;
                        }
                    }
                    for(let j in changeMap) {
                        const v = Number(changeMap[j]);
                        const k = Number(j)
                        let actors = [];
                        if(side_effect.scope === "target") {
                            actors = [target];
                        }
                        else if(side_effect.scope === "user") {
                            actors = [subject];
                        }
                        for(const a of actors) {
                            if(Math.random() <= side_effect.probability) {
                                if(k!=0) {
                                    a.removeState(k);
                                }
                                if(v!=0) {
                                    a.addState(v);
                                }
                            }
                        }
                    }
                    
                }
                else if (side_effect.type === "changeSkill") {
                    if(side_effect.perform_original_skill ) {
                        BattleManager.invokeAction(subject, target, false);
                        const realTarget = BattleManager._action.getActualTarget();
                        BattleManager._logWindow.popupDamage(realTarget);
                        // BattleManager._logWindow.displayDamage(realTarget);
                        realTarget._sprite.setupDamagePopup();
                        original_action_applied = true;
                    }
                    if(side_effect.hasOwnProperty('on_condition')) {
                        const condition_script = (new Function('return ' + side_effect.on_condition))();
                        if(condition_script(target)) {
                            currentAction.setSkill(side_effect.to_skill)
                            currentAction.item().animationId = $dataSkills[side_effect.to_skill].animationId;
                            BattleManager._logWindow.showAnimation(subject, currentAction.makeTargets().clone(), $dataSkills[side_effect.to_skill].animationId);
                        }
                    }
                    else {
                        let ids = targetStateIds;
                        if(side_effect.state_of === "user") {
                            ids = subjectStateIds;
                        }
                        const slow = false;
                        if(!side_effect.hasOwnProperty('on_state') ||  ids.includes(side_effect.on_state)) {
                            // slow version
                            if(slow) {
                                const action = new Game_Action(subject);
                                action.setSkill(side_effect.to_skill);
                                action.setItemObject($dataSkills[side_effect.to_skill]);
                                const targets = BattleManager._targets;
                                BattleManager._subject = subject;
                                subject._actions[0] = action;
                            }
                            else {
                                const action = new Game_Action(subject);
                                action.setSkill(side_effect.to_skill);
                                action.setItemObject($dataSkills[side_effect.to_skill]);
                                subject._actions[0] = action;

                                let targets = [target]
                                targets = targets.concat(BattleManager._targets);
                                for(let target of targets ) {
                                    let realTarget = null;
                                    for(let i = 0; i < action.numRepeats(); ++i ) {
                                        BattleManager._action = action;
                                        BattleManager.invokeAction(subject, target, false, false);
                                        realTarget = BattleManager._action.getActualTarget();
                                        BattleManager._logWindow.popupDamage(realTarget);
                                        //BattleManager._logWindow.displayDamage(realTarget);
                                        BattleManager._logWindow.displayAffectedStatus(realTarget);
                                        //BattleManager._logWindow.displayFailure(realTarget);
                                        if(realTarget) {
                                            realTarget._sprite.setupDamagePopup();
                                        }
                                        
                                    }
                                }
                                if(side_effect.perform_newskill_animation) {
                                    BattleManager._logWindow.showAnimation(subject, targets, action.item().animationId);
                                }
                                subject.clearActions();
                                BattleManager.endAction();
                            }
                            
                            // we don't need to apply action again if other state chagne need
                            original_action_applied = true;
                            ret = true;
                        }
                    }
                }
                else if (side_effect.type === "addEnemy") {
                    const index = currentAction.subject().index()
                    const battleSpriteSet = SceneManager._scene.children[0];
                    const lastIndex = SceneManager._scene.children[0];
                    let posX = 300 + Math.random() * 50.0;
                    let posY = 250 + Math.random() * 200.0;
                    // battleSpriteSet._enemySprites[index]._homeX 
                    let limitation = 20;
                    if(side_effect.hasOwnProperty('limit')) {
                        limitation = +side_effect.limit;
                    }
                    if($gameTroop._enemies.length < limitation) {
                        
                        const enemySprite = currentAction.subject().getBattleSprite();
                        const x = enemySprite._homeX;
                        const y = enemySprite._homeY;
                        if(side_effect.offset_x !== undefined) {
                            posX = side_effect.offset_x + x;
                        }
                        if(side_effect.offset_y !== undefined) {
                            posY = side_effect.offset_y + y;
                        }
                        const newEnemy = new Game_Enemy(+side_effect.enemy_id, posX, posY)
                        $gameTroop._enemies.push(newEnemy)
                        battleSpriteSet.addEnemy(newEnemy)
                        newEnemy.setAlly(false)
                        newEnemy.setSummonOwner(currentAction.subject())
                        target.result().success = true;
                        ret = true;
                    }
                    else {
                        GameInfoHelper.battleLog(`Seems the enemy summon failed!`);
                        
                    }
                    
                }
                // useless
                else if (side_effect.type === "transform") {
                    const enemy = currentAction.subject()
                    if(enemy.transform) {
                        enemy.transform(+side_effect.enemy_id);
                        enemy.revive()
                        enemy.setHp(enemy.mhp);
                    }
                }
                // Global event type
                else if (side_effect.type === "captureMonster") {
                    const success_script = (new Function('return ' + side_effect.success))();
                    const a = currentAction.subject();
                    const b = target;
                    const c = GameInfoHelper.getNoteObject(target.enemy(), "monster_level");
                    // default level
                    if (c === null) {
                        c = 99;
                    }
                    const need_items = side_effect.hasOwnProperty('need_items') ? side_effect.need_items : [];
                    let meetNeedItem = true;
                    if(need_items.length > 0) {
                        meetNeedItem = false;
                        for(let item_id of need_items) {
                            if($gameParty.hasItem($dataItems[item_id])) {
                                meetNeedItem = true;
                                break;
                            }
                        }
                    }
                    let captureSuccess = false;
                    if(success_script(a, b, c) && meetNeedItem) {
                        const enemyId = target.enemy().id;
                        if($myMonsterBallIndex.hasOwnProperty(enemyId)) {
                            const monsterBallId = $myMonsterBallIndex[enemyId]
                            target.addState(target.deathStateId());
                            target.performCollapse();
                            $gameParty.gainItem($dataItems[monsterBallId], 1);
                            // lose needed items
                            for(let item_id of need_items) {
                                $gameParty.loseItem($dataItems[item_id], 1);
                            }
                            GameInfoHelper.battleLog(`${target.enemy().name} is captured in <${$dataItems[monsterBallId].name}>!`);
                            captureSuccess = true;
                        }
                    }

                    if(!captureSuccess) {
                        GameInfoHelper.battleLog(`Seems the enemy capture failed!`);
                    }
                }
                
            }
        }
        const addSkillFunc = (subject, skillId, )=> {
            const action = new Game_Action(subject);
            action.setSkill(skillId);
            action.apply(subject);

            // add text in log
            BattleManager._logWindow.push("addText", `${subject.name()} suffer continous Frozen damage while action`);
            BattleManager._logWindow.displayActionResults(subject, subject);
        };

        for (let i in targetStateIds) {
            const targetStateId = targetStateIds[i];
            const targetState = targetStates[i];

            // if changeSkill
            const changeSkillObject = GameInfoHelper.getNoteObject(targetState, "changeSkill");
            const addSkillObject = GameInfoHelper.getNoteObject(targetState, "addSkill");
            const delaySkillObject = GameInfoHelper.getNoteObject(targetState, "delaySkill");
            // if redirect
            const redirectObject = GameInfoHelper.getNoteObject(targetState, "redirect");

            if(addSkillObject) {
                // addSkillFunc(target, addSkillObject.add_skill_id);
                // ret = false;
                // if(addSkillObject.remove_state) {
                //     target.removeState(targetStateId);
                // }
            }
            else if(redirectObject) {
                const canRedirect = (skill, redirectObject) => {
                    const skillOk = redirectObject.required_skills.length > 0 ? redirectObject.required_skills.includes(skill.id) : true;
                    const damageTypeOk =  redirectObject.required_damage_elementids.length > 0 ? redirectObject.required_damage_elementids.includes( skill.damage.elementId ) : true;
                    return skillOk && damageTypeOk;
                };
                if(redirectObject && canRedirect(skill, redirectObject) ) {
                    // do redirect
                    // perform action as usual
                    const dryrun = redirectObject.hasOwnProperty('dryrun_original_action') ? redirectObject['dryrun_original_action'] : false;
                    const result = currentAction.apply(target, dryrun);
                    const shouldShowOriginalAction = redirectObject.hasOwnProperty('show_original_action') ? redirectObject.show_original_action : true;
                    const originalIsHit = result.isHit();
                    const originalHpDamage = result.hpDamage;
                    
                    if(shouldShowOriginalAction) {
                        showHpDamageAndClearResult(target);
                    }

                    const redirectStateId = redirectObject.hasOwnProperty('target_state_id') ? redirectObject.target_state_id : null;
                    const redirectType = redirectObject.type

                    // get bond target
                    if(!redirectObject.hasOwnProperty('include_target_itself')) {
                        redirectObject['include_target_itself'] = false;
                    }
                    // redirect action, note it's relative to target
                    const action = new Game_Action(target);
                    let actors = []
                    if(redirectObject.scope === "enemy") {
                        actors = action.opponentsUnit().aliveMembers();
                    }
                    else if (redirectObject.scope === "ally") {
                        actors = action.friendsUnit().aliveMembers();
                    }
                    else if (redirectObject.scope === "user") {
                        actors = [action.subject()]
                    }
                    else if (redirectObject.scope === "target") {
                        actors = [subject]
                    }
                    else if (redirectObject.scope === "phantom_owner") {
                        if(target instanceof Game_Enemy && target.isPhantom()) {
                            const phantomOwner = target.getPhantomTarget()
                            if(phantomOwner !== null && phantomOwner.isAlive()) {
                                actors = [phantomOwner]
                            }
                        }
                    }

                    // no set: default true
                    const remove_state_postredirect = redirectObject.hasOwnProperty('remove_state_postredirect') ? redirectObject.remove_state_postredirect : true;
                    
                    for(let actor of actors) {
                        if((GameInfoHelper.isSameActor(target, actor) && !redirectObject.include_target_itself) 
                            ||  (redirectStateId !== null && !actor._states.includes(redirectStateId)) ) {
                            continue;
                        }
                        if(redirectType == "damage") {
                            if(originalIsHit) {
                                const damage = originalHpDamage;

                                if(remove_state_postredirect) {
                                    target.removeState(targetStateId)
                                }

                                // TODO: here we just use pure damage skill, please don't change this skill
                                action.setOverrideFormula(()=>{
                                    return damage;
                                });
                                action.setSkill(4);
                                action.apply(actor);

                                // add text in log
                                BattleManager._logWindow.push("addText", TextManager.actorDamage.format(actor.name(), damage));
                                
                                // no sound
                                showHpDamageAndClearResult(actor, false);
                            }
                        }
                        else if(redirectType == 'heal') {
                            
                            if(originalIsHit && originalHpDamage > 0) {
                                const heal = Math.max(0, originalHpDamage);

                                if(remove_state_postredirect) {
                                    target.removeState(targetStateId)
                                }
                                target.result().clear()

                                // TODO: refactor code above and here
                                action.setOverrideFormula(()=>{
                                    return -heal;
                                });
                                action.setSkill(5);
                                action.apply(actor);

                                // just play sound
                                // BattleManager._logWindow.push("performReflection", target);
                                // add text in log
                                BattleManager._logWindow.displayActionResults(actor, actor);
                                // currentAction.item().animationId = $dataSkills[5].animationId;
                                BattleManager._logWindow.showAnimation(actor, action.makeTargets().clone(), $dataSkills[5].animationId);
                            }
                            // redirect failed
                            else {
                                if(dryrun) {
                                    // we shouldn't dry run
                                    const result = currentAction.apply(target);
                                }
                                
                                if(!shouldShowOriginalAction) {
                                    BattleManager._logWindow.displayActionResults(subject, target);
                                }
                            }

                        }

                        if (actor.isDead()) {
                            actor.performCollapse();
                        }
                        
                    }

                    if (target.isDead()) {
                        target.performCollapse();
                    }

                    ret = true;
                }
            }
        }

        // subject states
        //for(let i in subjectStateIds) {
        //    const subjectState = subjectStates[i];
        //    const subjectStateId = subjectStateIds[i];
        //    const onSkill = GameInfoHelper.getNoteObject(subjectState, "onSkill");
        //    //TODO: it's not convenient to go to note to code unless we really need to support more general behavior
        //    if(onSkill !== null) {
        //        if( !onSkill.hasOwnProperty("skill") || onSkill.skill == skill.id || onSkill.exclude_skill != skill.id) {
        //            if(onSkill.behavior === "add_skill") {
        //                addSkillFunc(subject, onSkill.add_skill_id);
        //                ret = false;
        //            }

        //            if(onSkill.remove_state) {
        //                subject.removeState(subjectStateId);
        //            }
        //        }

        //    }
        //}

        return ret;
    }
}
