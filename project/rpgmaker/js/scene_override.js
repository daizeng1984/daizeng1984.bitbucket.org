const CUSTOME_EVENT_POSITION_SLOT_INDEX = 4000;
const CUSTOME_EVENT_POSITION_SLOT_START = 4001;
const CUSTOME_EVENT_POSITION_SLOT_END = 4050;
class SceneOverride {
    static overrideSceneObject(scene, mapId) {
        const events = scene.events

        // load customized event to scene
        for(let row = CUSTOME_EVENT_POSITION_SLOT_START; row <= CUSTOME_EVENT_POSITION_SLOT_END; ++row) {
            const data = $gameVariables._data[row];
            if(data && typeof data === "string" ) {
                const eventObj = JSON.parse(data);
                if(eventObj.hasOwnProperty('mapId') && eventObj.mapId == mapId) {
                    events[eventObj.id] = eventObj;
                    // update scene to load this new sprites
                    //SceneManager._scene.children[0].addCharacter(e);
                    //
                }
            }
        }
    }
    static addCustomEvents(e, mapId) {
        let index = $gameVariables._data[CUSTOME_EVENT_POSITION_SLOT_INDEX];
        if(typeof index !== "number") {
            index = 0;
        }
        ++index;
        index = index % (CUSTOME_EVENT_POSITION_SLOT_END - CUSTOME_EVENT_POSITION_SLOT_START + 1)
        // add this won't hurt
        e.mapId = mapId;
        e.id = $dataMap.events.length;
        $dataMap.events[e.id] = e;
        if(index >= 0 && index <= CUSTOME_EVENT_POSITION_SLOT_END - CUSTOME_EVENT_POSITION_SLOT_START) {
            $gameVariables._data[index + CUSTOME_EVENT_POSITION_SLOT_START] = JSON.stringify(e);
        }
        $gameVariables._data[CUSTOME_EVENT_POSITION_SLOT_INDEX] = index;
        $gameMap.addEvent(e);
        //SceneManager._scene._mapLoaded = false;

        const prevSpeed = SceneManager._scene.fadeSpeed();
        $gamePlayer.reserveTransfer(mapId, $gamePlayer.x, $gamePlayer.y, $gamePlayer.direction(), 0);
    }
}

class FullscreenFontEffect {
    static fadeIn(speed) {
        FullscreenFontEffect.animate(-speed);
    }
    static fadeOut(speed) {
        FullscreenFontEffect.animate(speed);
    }
    static animate(speed = 0.01) {
        const id = 'zoomblur';
        const filterType = 'zoomblur';
        const targetType = Filter_Controller.targetType.FullScreenWithWindow;
        const posRefTargetId = -1; //player

        if(!$gameMap.getFilterController(id)) {
            $gameMap.createFilter(id, filterType, targetType, [], posRefTargetId);
        }

        $gameMap.enableFilter(id, true);

        let intense = speed > 0 ? 0.0 : 1.0;

        const intervalId = setInterval(()=>{

            $gameMap.setFilter(id, [0, 0, 0, intense]);
            try {
                SceneManager._scene._messageWindow.alpha = 1.0 - intense

            intense += speed;

            if( (speed < 0 && intense <= 0.0) || (speed > 0 && intense >= 1.0)) {
                clearInterval(intervalId);
                $gameMap.enableFilter(id, false);
                SceneManager._scene._messageWindow.alpha = speed > 0 ? 0 : 1;
            }

            }
            catch(e) {
                clearInterval(intervalId);
            }

        }, 30);
    }

};

