const PresetMenuScript = {
    10:  // mimic
    function(that){
        that._data = []
        if(that._actor) {
            const currentSummon = that._actor.getCurrentSummon();
            if (currentSummon !== null) {
                that._data = currentSummon.enemy().actions.reduce((r, a)=> {
                    if(a.hasOwnProperty('skillId') && a.skillId > 10) {
                        return r.concat($dataSkills[a.skillId]);
                    }
                    return r;
                }, []);
            } 
        }
        return true;
    },
};


