

const Art_Game_Map_setup = Game_Map.prototype.setup;
Game_Map.prototype.setup = function(mapId) {
    Art_Game_Map_setup.call(this, mapId);
    const defBattleBgm = $dataSystem.battleBgm;
    if ($dataMap.meta.battleBGM) {
        $gameSystem.setBattleBgm({name: $dataMap.meta.battleBGM.trim(), pitch: defBattleBgm.pitch, volume: defBattleBgm.volume});
    } else {
        $gameSystem.setBattleBgm(defBattleBgm);
    }
};

