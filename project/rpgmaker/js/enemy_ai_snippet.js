const attackAnyoneAndWild = function(actions, enemy, troop, party){
    for(const a of enemy._actions) {
        a._targetOverride = function(that) {
            const everyone = that.targetsForEveryone();
            const selfIndex = everyone.indexOf(enemy);
            if(selfIndex >= 0) {
                everyone.splice(selfIndex, 1);
            }
            if(everyone.length === 0) {
                return everyone;
            }
            return [everyone[Math.floor(Math.random()*everyone.length)]];
        }
    }
    enemy.setVictoryRequired(false);
    return actions;
};

const transformOnTimer = function(actions, enemy, timer, skillId){
    if(!enemy.hasOwnProperty('transformTimer')) {
        enemy.transformTimer = timer;
    }
    if (--enemy.transformTimer  == 0) {
        enemy.transformTimer = timer;

        return [{
            conditionParam1: 0,
            conditionParam2: 0,
            conditionType: 0,
            rating: 5,
            skillId,
        }];
    }
    return actions
};

// Linear (no easing)
function linear(t) {
    return t;
}

// Ease-In (slow start, fast end)
function easeIn(t) {
    return t * t;
}

// Ease-Out (fast start, slow end)
function easeOut(t) {
    return t * (2 - t);
}

// Ease-In-Out (slow start, fast middle, slow end)
function easeInOut(t) {
    return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
}


function animateScale(currentScale, targetScale, duration, easingFunction, onUpdate, onComplete) {
    const startTime = performance.now();
    const scaleDifference = targetScale - currentScale;

    function step(timestamp) {
        const elapsed = timestamp - startTime;
        const progress = Math.min(elapsed / duration, 1); // Normalize progress (0 to 1)
        const easedProgress = easingFunction(progress); // Apply easing function
        const newScale = currentScale + scaleDifference * easedProgress;


        onUpdate(newScale);

        if (progress < 1) {
            requestAnimationFrame(step); // Continue the animation
        } else if (onComplete) {
            onComplete(); // Call onComplete when animation is done
        }
    }

    requestAnimationFrame(step); // Start the animation
}

const PresetHandMadeAI = {
    8: // death eye
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // tp regen
            enemy.addNewState(45)
        }
        return actions
    },
    10: // mpsliem
    attackAnyoneAndWild,
    11: // tpsliem
    attackAnyoneAndWild,
    13: // dark matter skeleton
    function(actions, enemy, troop, party){
        return transformOnTimer(actions, enemy, 4, 438);
    },
    14: // quick silver
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() < 2) {
            // silver scratch and silver needle
            return actions.filter(x => !(x.skillId == 42 && x.skillId == 43));
        }
        if(!enemy._states.includes(62)) {
            if(enemy.darkcrystalCount === undefined) {
                enemy.darkcrystalCount = 0;
            }
            if(++enemy.darkcrystalCount >= 3) {
                return [{
                    conditionParam1: 0,
                    conditionParam2: 0,
                    conditionType: 0,
                    rating: 5,
                    skillId: 50, // silver needle hp = 1
                }];

            }else{
                return [{
                    conditionParam1: 0,
                    conditionParam2: 0,
                    conditionType: 0,
                    rating: 5,
                    skillId: 339, // dark crytalize II
                }];
            }
        }
        return actions
    },
    28: // goblin captain/lead
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // dark armor I
            enemy.addNewState(51)
        }
        return actions
    },
    36: // cruzwave
    function(actions, enemy, troop, party){
        // has reflection for ever
        enemy.addNewState(18);
        return actions
    },
    49: // dark matter rotten skeleton
    function(actions, enemy, troop, party){
        return transformOnTimer(actions, enemy, 3, 439);
    },
    52: // goblin sergeant
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // dark armor I
            enemy.addNewState(51)
            // tp regen
            enemy.addNewState(45)
        }
        return actions
    },
    55: // boss cherry bomb
    function(actions, enemy, troop, party){
        if(!enemy.hasOwnProperty('deathCountDown')) {
            enemy.deathCountDown = 2;
        }
        // scale it based on counter
        const battleSpriteSet = SceneManager._scene.children[0];
        const sprite = battleSpriteSet.getEnemySprite(enemy);
        if(sprite !== null) {
            const rate = (3 - enemy.deathCountDown) / 3.0;
            const scale = rate * 2.0  + (1.0 - rate)* 1.0;
            animateScale(sprite.scale.x, scale, 500, easeOut, (newScale)=>{
                sprite.scale.x = newScale;
                sprite.scale.y = newScale;
            }, ()=>{});
        }
        

        if(enemy.deathCountDown === 0) {
            return [{
                conditionParam1: 0,
                conditionParam2: 0,
                conditionType: 0,
                rating: 5,
                skillId: 237, // Bang I
            }];
        }
        --enemy.deathCountDown;

        return actions
    },
    63: // gabriel fight boss \N[200]
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // dark armor II
            enemy.addNewState(52)
        }
        return actions
    },
    69: // water crab
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // harden II
            enemy.addNewState(57)
        }
        return actions
    },
    72: // shade minion
    function(actions, enemy, troop, party){
        const allAliveOpponents = enemy.opponentsUnit().aliveMembers()
        let weakestIdx = GameInfoHelper.query(allAliveOpponents, x=>(x._hp - 1 + 10)%10, m=>true)[0].index;

        // 80% to use this AI
        if( Math.random() > 0.2) {
            if ((allAliveOpponents[weakestIdx]._hp - 1 + 10)%10 <= 1) {
                for(let a of enemy._actions) {
                    let index = allAliveOpponents[weakestIdx].index();
                    a.setTarget(index);
                }
                // only attack with ro's surgeon
                return actions.filter(x => x.skillId == 353);
            }
        }
        return actions
    },
    75: // dark matter shadow skeleton
    function(actions, enemy, troop, party){
        return transformOnTimer(actions, enemy, 3, 300);
        // if(!enemy.hasOwnProperty('transformTimer')) {
        //     enemy.transformTimer = 3;
        // }
        // if (--enemy.transformTimer  == 0) {
        //     enemy.transformTimer = 3;
        //     return enemy.enemy().actions.filter(x => x.skillId == 300);
        // }
        // return actions
    },
    77:  // shadow knight
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // dark armor I
            enemy.addNewState(51)
        }
        
    // useful information
    const allBattleMembers = $gameParty.allBattleMembers();
    const allEnemies = $gameTroop._enemies;
    let weakestIdx = GameInfoHelper.query(allBattleMembers, x=>x._hp, m=>true)[0].index;
    const strongestMemberInfo = GameInfoHelper.query(allBattleMembers, (x)=>x.atk, (m)=>true).slice(-1)[0];
    let aliveMemberInfo = GameInfoHelper.query(allBattleMembers, (x)=>0, x=>x.isAlive());
    // now here's the FSM style AI

    // default weakest
    for(let a of enemy._actions) {
        a.setTarget(weakestIdx);
    }

    // Very likely to attack strongest
    if(GameInfoHelper.byChanceOf(50)) {
        const ATTACK_BUFF_IDX = 2; // 0 maxHP 1 maxMP 2 Attack ...
        const debuffedMemberInfo = aliveMemberInfo.filter(x=>x.member._buffs[ATTACK_BUFF_IDX] < 0 );
        let strongestIdx = strongestMemberInfo.index;
        let strongestActor = strongestMemberInfo.member;
        if(debuffedMemberInfo.length == 0 && aliveMemberInfo.length >= 3 && strongestActor._buffs[ATTACK_BUFF_IDX] >= 0) {
            console.log(`targeting strongest member: ${strongestMemberInfo.member._name}`);
            for(let a of enemy._actions) {
                a.setTarget(strongestIdx);
            }
            // only attack with thrill attack
            return actions.filter(x => x.skillId == 247);
        }
    }
    
    // for a chance, it'll attack silent attack on mage
    if(GameInfoHelper.byChanceOf(50)) {
        // mages: classId is either 2 or 3
        const mageInfo = GameInfoHelper.query(allBattleMembers, x=>x._mp, x=>x.isAlive() && (x._classId == 2 || x._classId == 3));
        
        if(mageInfo.length > 0 && !GameInfoHelper.actorHasStates(mageInfo[0].member, 6)) {
            const mageIdx = mageInfo[0].index;
            console.log(`targeting mage member: ${mageInfo[0].member._name}`);

            for(let a of enemy._actions) {
                a.setTarget(mageIdx);
            }
            // silent attack
            return actions.filter(x => x.skillId == 251);
        }
    }

    let otherEnemyInfo = GameInfoHelper.query(allEnemies, x=>x._hp, e=>e.isAlive() && e._enemyId < enemy._enemyId);
    if(GameInfoHelper.byChanceOf(50)) {
        // params[0] is HP
        if (enemy._hp < enemy.enemy().params[0]*0.2 && otherEnemyInfo.length > 0 && !GameInfoHelper.actorHasStates(enemy, 83)) {
            console.log(`targeting enemy member: ${otherEnemyInfo[0].member.enemy().name}`);
            //enemy.addState(83); // dark absorb
            const absorbIdx = otherEnemyInfo[0].index
            for(let a of enemy._actions) {
                a.setTarget(absorbIdx);
            }
            return [GameInfoHelper.newNonGameAction(255)];
        }
    }
    console.log(`random actions`);
    return actions.filter(x => x.skillId != 251);
},

    76: // shadow mage
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // dark armor I
            enemy.addNewState(51)
        }
        
    // useful information
    const allBattleMembers = $gameParty.allBattleMembers();
    const allEnemies = $gameTroop._enemies;
    let weakestIdx = GameInfoHelper.query(allBattleMembers, x=>x._hp, m=>true)[0].index;
    const strongestMemberInfo = GameInfoHelper.query(allBattleMembers, (x)=>x.atk, (m)=>true).slice(-1)[0];
    let aliveMemberInfo = GameInfoHelper.query(allBattleMembers, (x)=>0, x=>x.isAlive());

    // now here's the FSM style AI
    if(enemy.hasOwnProperty("darkCounter") && enemy.darkCounter >= 5) {
        enemy.darkCounter = 0;
        GameInfoHelper.battleLog(`${enemy.enemy().name}'s dark magic is going to burst!`);
        return [GameInfoHelper.newNonGameAction(365)]; //darkness IV
    }
    let otherEnemyInfo = GameInfoHelper.query(allEnemies, x=>x._hp, e=>e.isAlive() && e != enemy );
    if(GameInfoHelper.byChanceOf(10)) {
        // params[0] is HP
        if (otherEnemyInfo.length > 0) {
            console.log(`targeting enemy member: ${otherEnemyInfo[0].member.enemy().name}`);
            const absorbIdx = otherEnemyInfo[0].index
            for(let a of enemy._actions) {
                a.setTarget(absorbIdx);
            }
            return [GameInfoHelper.newNonGameAction(258)];
        }
    }
    console.log(`normal actions`);
    if(!enemy.hasOwnProperty("darkCounter")) {
        enemy.darkCounter = 0;
    }
    enemy.darkCounter++;
    GameInfoHelper.battleLog(`${enemy.enemy().name}${enemy._letter} is boosting dark magic to level ${enemy.darkCounter}!`);
    return actions;
},
    93: // fairy
    function(actions, enemy, troop, party){
        const allAliveFriends = enemy.friendsUnit().aliveMembers().filter(a => ! (a instanceof Game_Enemy))
        // minimal HP of group
        const needHealingMemberInfo = GameInfoHelper.query(allAliveFriends, (x)=>x.hp, (m)=>true)[0];
        if(needHealingMemberInfo.member.hp < 0.4 * needHealingMemberInfo.member.mhp) {
            for(let a of enemy._actions) {
                a.setTarget(needHealingMemberInfo.member.index());
            }
            // heal III
            return actions.filter(x => x.skillId == 54);
        }
        else {
            // recover II
            return actions.filter(x => x.skillId == 57);
        }
        return actions;
    },    
    95: // fairy II
    function(actions, enemy, troop, party){
        const allAliveFriends = enemy.friendsUnit().aliveMembers().filter(a => ! (a instanceof Game_Enemy))
        // minimal HP of group
        const needHealingMemberInfo = GameInfoHelper.query(allAliveFriends, (x)=>x.hp, (m)=>true)[0];
        if(needHealingMemberInfo.member.hp < 0.4 * needHealingMemberInfo.member.mhp) {
            for(let a of enemy._actions) {
                a.setTarget(needHealingMemberInfo.member.index());
            }
            // heal III
            return actions.filter(x => x.skillId == 54);
        }
        else {
            // recover III
            return actions.filter(x => x.skillId == 58);
        }
    },    
    98: // Loo I
    function(actions, enemy, troop, party){
        const allAliveOpponents = enemy.opponentsUnit().aliveMembers()
        const phantomMemberInfo = GameInfoHelper.query(allAliveOpponents, (x)=>x.hp, (m)=>m.isPhantom());
        if(enemy.tp < 5) {
            actions = actions.filter(x => x.skillId != 67);
        }
        if(phantomMemberInfo.length > 0) {
            for(let a of enemy._actions) {
                a.setTarget(phantomMemberInfo[0].member.index());
            }
        }
        return actions;
    },    
    109: // thunder stone
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // harden I
            enemy.addNewState(56)
        }
        return actions
    },    
    114: // thunder golem
    function(actions, enemy, troop, party){
        if($gameTroop.turnCount() === 0) {
            // harden I
            enemy.addNewState(56)
            // tp regen
            enemy.addNewState(45)
        }
        return actions
    },    
}
