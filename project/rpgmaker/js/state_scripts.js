const frozen_damage = (subject, stateId, skillId) => {
    const action = new Game_Action(subject);
    const prevResult = subject._result;
    // ice
    if(prevResult.action && !prevResult.physical && prevResult.action.item() && prevResult.action.item().damage.elementId === 3 ) {
        // no breaking yet
    }
    else {
        subject._result = new Game_ActionResult();
        action.setSkill(skillId);
        action.apply(subject);
        // add text in log
        BattleManager._logWindow.push("addText", `${subject.name()} suffer extra Frozen break damage.`);
        BattleManager._logWindow.popupDamage(subject);
        BattleManager._logWindow.displayDamage(subject);
        BattleManager._logWindow.displayAffectedStatus(subject);
        BattleManager._logWindow.displayFailure(subject);

        subject._sprite.setupDamagePopup();
        // prevResult.hpDamage += subject._result.hpDamage;
        // prevResult.mpDamage += subject._result.mpDamage;
        subject._result = prevResult;
        subject._result.used = false;
        subject.startDamagePopup();
        
        // BattleManager._logWindow.displayActionResults(null, subject);
    }
};

const resistForTurns = function(subject, stateId, t) {
    const executed = subject.removeStateOriginal(stateId);

    if (executed) {
        // current turn  = $gameTroop._turnCount
        const turns = $gameTroop._turnCount;
        subject._state_resist_turn = turns + t;
    }
    return true;
};

const resistForTurnsWithDamage = function(subject, stateId, t, skillId) {
    const executed = subject.removeStateOriginal(stateId);

    if (executed) {
        // current turn  = $gameTroop._turnCount
        const turns = $gameTroop._turnCount;
        subject._state_resist_turn = turns + t;

        // damage
        frozen_damage(subject, stateId, skillId);
    }
    return true;
    
}

const PresetStateOnRemvoe = {
    137:  // heavy stun
    function(subject, stateId) {
        return resistForTurns(subject, stateId, 5);
    },
    // Frozen I~IV
    133:  // frozen
    function(subject, stateId) {
        return resistForTurnsWithDamage(subject, stateId, 5, 413);
    },
    134:
    function(subject, stateId) {
        return resistForTurnsWithDamage(subject, stateId, 5, 414);
    },
    135:
    function(subject, stateId) {
        return resistForTurnsWithDamage(subject, stateId, 5, 415);
    },
    136:
    function(subject, stateId) {
        return resistForTurnsWithDamage(subject, stateId, 5, 416);
    },
    155:
    function(subject, stateId) {
        subject.removeStateOriginal(stateId);
        // die
        subject.addNewState(subject.deathStateId());
        subject.performCollapse();
    },
    394:
    function(subject, stateId) {
        $gameParty.removeActor(3);
        $gameMessage.setFaceImage('People1', 4)
        $gameMessage.setSpeakerName('Saton')
        $gameMessage.add(`I, \\N[3], decided to leave the team! I'm sorry that my decision massively lower the whole team's average power.`)
        $gameSwitches.setValue(33)
        subject.removeStateOriginal(stateId);
        return true;
    },
};

const noAddReset = function(subject, stateId) {
    if (subject.isStateAddable(stateId)) {
        const affected = subject.isStateAffected(stateId);
        if (!affected) {
            subject.addNewState(stateId);
            subject.refresh();
            subject.resetStateCounts(stateId);
            subject._result.pushAddedState(stateId);
        }
    }
    return true;
};

const noAddResetWithResist = function(subject, stateId) {
    if (subject.isStateAddable(stateId)) {
        if(subject._state_resist_turn === undefined || subject._state_resist_turn <= $gameTroop._turnCount) {
            const affected = subject.isStateAffected(stateId);
            if (!affected) {
                subject.addNewState(stateId);
                subject.refresh();
                subject.resetStateCounts(stateId);
                subject._result.pushAddedState(stateId);
            }
        }
    }
    return true;
};

const PresetStateOnAdd = {
    1: // dead
    function(subject, stateId) {
        // if subject has passive skill 494 NiePan
        // TODO: make it like canRejuvenation etc.
        if(subject instanceof Game_Actor && subject.hasSkill(494) && !subject._hasRejuvenation) {
            subject.removeState(1); // add state revived 162
            subject._hasRejuvenation = true;
            // revive and message
            subject.setHp(1);
            $gameTemp.requestAnimation([subject], $dataSkills[494].animationId);
            subject.addState(162);
            return true;
        }
        return false;
    },

    137:  // super heavy stun
    noAddResetWithResist,
    108: // doomed no reset
    noAddReset,
    // frozen I~IV
    133: noAddResetWithResist,
    134: noAddResetWithResist,
    135: noAddResetWithResist,
    136: noAddResetWithResist,
    162:
    function(subject, stateId) {
        // original state
        subject.addStateOriginal(stateId);
        // calculate the last hp for revived buff
        const lastHp = subject.getLastHpBeforeDead();
        const ratio = lastHp / subject.mhp;
        subject.setExtraBuffPlus(0.25 * (1 - ratio) + 2.5 * ratio);
    },
};

