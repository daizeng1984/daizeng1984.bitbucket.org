const PresetItemScript = {
    71:  // magic lense
    function(actions){
        // useful information
        if(actions._targetIndex >= 0 && actions._targetIndex < $gameTroop._enemies.length) {
            const target = $gameTroop._enemies[actions._targetIndex];

            // element info
            let elementInfo = '';
            const colorCoding = (value) => {
                if(value > 1.2) {
                    return 3;
                }
                else if(value > 1.0) {
                    return 0;
                }
                else if(value < 0.8) {
                    return 2;
                }
            }
            // resisit
            let resistInfo = '';

            for(const t of target.allTraits()) {
                if(t.code === 11) {
                    if(t.dataId >= 2 && t.dataId <= 10) {
                        elementInfo += `\\I[${t.dataId + 16*4 - 2}]\\C[${colorCoding(t.value)}]${t.value == 0 ? '-' : t.value}\\C[0]`;
                    }
                }
                else if(t.code === 14) {
                    if(t.dataId >= 4 && t.dataId <= 13) {
                        resistInfo += `\\I[${$dataStates[t.dataId].iconIndex}]`;
                    }
                }
            }

            const description = GameInfoHelper.getNoteObject(target.enemy(), 'description') ?? "N/A";

            $gameMessage.add(`Target Name: ${target.name()}, HP: ${target.hp}/${target.mhp}, MP:${target.mp}/${target.mmp}, Atk: ${target.atk}, Def: ${target.def}, Mat: ${target.mat}, Mdf:${target.mdf}, Elements Effect: ${elementInfo}, Resists: ${resistInfo}, Info: ${description}`);
        }
    },
};

//-----------------------------------------------------------------------------
// Scene_SkillEx
//
// The scene class to allow do something extra I need, the naming is bad:) now I just hack it for one case
// future might extended to be more flexible
/* example
$showUseSkillMenuScene((that, base, item)=>{
        if([100, 101].includes(item.id)) {
            return that.actor().canPaySkillCost(item);
        }
        else if(item.id == 172) {
            const blazingSword = $dataWeapons[50];
            const armed = that.actor().weapons().includes(blazingSword)
            const abletoArm = that.actor().canEquip(blazingSword)
            return that.actor().canPaySkillCost(item) && ( armed || abletoArm );  // only Reid and we have BlazingSword can do
        }
        return false;

    }, 
    (ok, item, action)=>{
        if(ok) {
            $gameMessage.add(`${action.subject().name()} performed ${item.name}`);
            if(action.subject().actorId() === 1) {
                this.command212([0, 8, true]);
            }
            else {
                this.command212([0, 66, true]);
            }
            this.command123(['A', 0]); // Self Switch A = ON
        }
    });

 */
function $showUseSkillMenuScene(filters, callback = ()=>{}) {
    let usedSkill = false
    function TempScene() {
        this.initialize(...arguments);
    }
    TempScene.prototype = Object.create(Scene_SkillEx.prototype);
    TempScene.prototype.constructor = TempScene;

    TempScene.prototype.initialize = function() {
        Scene_SkillEx.prototype.initialize.call(this, filters, 
            (used, item, action)=>{
                callback(used, item, action)
            }
        );
    };

    SceneManager.push(TempScene);
}

function Scene_SkillEx() {
    this.initialize(...arguments);
}

Scene_SkillEx.prototype = Object.create(Scene_Skill.prototype);
Scene_SkillEx.prototype.constructor = Scene_SkillEx;

Scene_SkillEx.prototype.initialize = function(actionOverride, callback) {
    Scene_Skill.prototype.initialize.call(this);
    this.actionOverride = actionOverride ? actionOverride : (base)=>{return base};
    this.callback = callback;
};

Scene_SkillEx.prototype.create = function() {
    Scene_Skill.prototype.create.call(this);
};

//-----------------------------------------------------------------------------
// Window_SkillListEx

function Window_SkillListEx() {
    this.initialize(...arguments);
}

Window_SkillListEx.prototype = Object.create(Window_SkillList.prototype);
Window_SkillListEx.prototype.constructor = Window_SkillListEx;

Window_SkillListEx.prototype.initialize = function(rect, actionOverride, okCallback) {
    Window_SkillList.prototype.initialize.call(this, rect);
    this.actionOverride = actionOverride;
};

Window_SkillListEx.prototype.isEnabled = function(item) {
    const baseEnabled =  (this._actor && this._actor.canUse(item));
    return this.actionOverride(baseEnabled, item);
};

Scene_SkillEx.prototype.createItemWindow = function() {
    const rect = this.itemWindowRect();
    this._itemWindow = new Window_SkillListEx(rect, (base, item)=>{
        return this.actionOverride(this, base, item)
    });
    this._itemWindow.setHelpWindow(this._helpWindow);
    this._itemWindow.setHandler("ok", ()=>{
        this.actor().setLastMenuSkill(this.item());
        const action = new Game_Action(this.user());
        const item = this.item();
        action.setItemObject(item);
        this.useItem();
        this.activateItemWindow();
        this.popScene();
        this.callback(true, item, action)
    });
    this._itemWindow.setHandler("cancel", ()=>{
        this.popScene();
        this.callback(false);
    });
    this._skillTypeWindow.setSkillWindow(this._itemWindow);
    this.addWindow(this._itemWindow);
};



